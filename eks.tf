locals {
    cluster_name = "poc"
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "10.0.0"
  # insert the 4 required variables here

  cluster_name    = local.cluster_name
  cluster_version = "1.15"
  subnets         = module.vpc.public_subnets
  vpc_id          = module.vpc.vpc_id

  # Additional aws-auth configmap bindings
  # map_roles    = var.map_roles
  # map_users    = var.map_users
  # map_accounts = var.map_accounts

  # Custom tags
  tags = {
    Environment = local.cluster_name
    GithubRepo  = "terraform-aws-eks"
    GithubOrg   = "terraform-aws-modules"
  }

  node_groups = {
    ng_1 = {
      desired_capacity = 1
      max_capacity     = 5
      min_capacity     = 1

      instance_type = "t3.small"
      k8s_labels = {
        Environment = local.cluster_name
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
      }

      additional_tags = {
        Name = "${local.cluster_name}-ng_1"
      }
    }
  }
}
