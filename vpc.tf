data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.28.0"

  name               = "eks-poc"
  cidr               = var.vpc_cidr
  azs                = slice(data.aws_availability_zones.available.names, 0, 3)

  private_subnets    = [ "${cidrsubnet(var.vpc_cidr, 4, 0)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 1)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 2)}"
                       ]

  public_subnets     = [ "${cidrsubnet(var.vpc_cidr, 4, 10)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 11)}",
                         "${cidrsubnet(var.vpc_cidr, 4, 12)}"
                       ]

  enable_nat_gateway = true

  # Set tags required for alb-ingress-controller
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }
}
